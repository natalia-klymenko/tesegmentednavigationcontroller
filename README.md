# TESegmentedNavigationController #

Current Version: **0.14**

Project Status: Beta

---

Licensed under [MIT](https://en.wikipedia.org/wiki/MIT_License)

## Install methods ##

### Cocoapods ###

To install via [cocoapods](https://cocoapods.org/) include the following line:

```
pod 'TESegmentedNavigationController'
```

### Manually ###

Download the project and copy the contents of the folder `TESegmentedNavigationController` into your project.

Additionally you must install `TESegmentedControl` manually from: https://bitbucket.org/al_timellis/tesegmentedcontrol

## Issues/Bugs? ##

If you find any issues or bugs please feel free to use the **[Issues](https://bitbucket.org/al_timellis/tesegmentednavigationcontroller/issues?status=new&status=open)** tab in the repository to report it.
