//
//  NSAttributedString+TESegmentedNavigationController.h
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 11/07/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSAttributedString (TESegmentedNavigationController)

/**
 *	Creates an attributed string from a given string
 *
 *	@param string	The string to be converted
 */
+ (nonnull NSAttributedString *)attributedStringWithString:(nonnull NSString *)string;

/**
 *	Creates an attributed string from an image
 *
 *	@param image	The image to be converted
 */
+ (nonnull NSAttributedString *)attributedStringWithImage:(nonnull UIImage *)image;

@end
