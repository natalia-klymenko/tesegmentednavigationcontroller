//
//  NSAttributedString+TESegmentedNavigationController.m
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 11/07/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "NSAttributedString+TESegmentedNavigationController.h"

@implementation NSAttributedString (TESegmentedNavigationController)

+ (NSAttributedString *)attributedStringWithString:(NSString *)string {
	return [[self alloc] initWithString:string];
}

+ (NSAttributedString *)attributedStringWithImage:(UIImage *)image {
	NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
	imageAttachment.image = [image copy];
	return [self attributedStringWithAttachment:imageAttachment];
}

@end
