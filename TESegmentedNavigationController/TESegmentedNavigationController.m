//
//  TESegmentedNavigationController.m
//  TESegmentedNavigationController
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TESegmentedNavigationController.h"
#import "TESegmentedNavBarView.h"

@interface TESegmentedNavigationController () <UINavigationControllerDelegate, UIGestureRecognizerDelegate>

/**
 *	Controller specific left buttons
 *
 *	Holds a weak copy of the buttons that should be displayed to the left of the label for the currently visible view controller.
 *	
 *	Whenever the visible view controller changes this is reset to nil
 */
@property (nonatomic, nullable, weak) NSArray *vc_leftButtons;

/**
 *	Controller specific right buttons
 *
 *	Holds a weak copy of the buttons that should be displayed to the right of the label for the currently visible view controller.
 *
 *	Whenever the visible view controller changes this is reset to nil
 */
@property (nonatomic, nullable, weak) NSArray *vc_rightButtons;

/**
 *	The view controller associated with the vc_* objects
 */
@property (nonatomic, nullable, weak) UIViewController *vc_current;

@property (nonatomic, nonnull) NSLayoutConstraint *controlHeightConstraint;

@property (nonatomic) BOOL gestureActive;

@end

@implementation TESegmentedNavigationController

- (TESegmentedNavBarView *)navBar {
	if (![self.navigationBar isKindOfClass:[TESegmentedNavBarView class]]) {
		//Accessed via KVC. Best method to allow extensibility
		[self setValue:[[TESegmentedNavBarView alloc] initWithParent:self] forKey:@"navigationBar"];
		[((TESegmentedNavBarView *)self.navigationBar).segmentedControl addTarget:self action:@selector(segmentChanged:)
																 forControlEvents:UIControlEventValueChanged];
	}
	return (TESegmentedNavBarView *)self.navigationBar;
}

- (BOOL)isModal {
	return (self.presentingViewController.presentedViewController == self)
			|| (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
			|| [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
	if (self = [super initWithRootViewController:rootViewController]) {
		if (rootViewController) {
			[self setTitle:rootViewController.title];
		}
		self.gestureActive = NO;
	}
	return self;
}

- (void)segmentChanged:(TESegmentedControl *)sender {
	if (self.delegateSeg) {
		if ([self.delegateSeg respondsToSelector:@selector(onNavigationController:tabChanged:)]) {
			[self.delegateSeg onNavigationController:self tabChanged:sender];
		}
	}
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	super.delegate = self;
	self.navigationItem.title = @"";
	self.navigationItem.titleView.hidden = YES;
	[self updateNavActionsWithViewController:nil];
}

- (void)setTabState {
	BOOL tabShown = (self.viewControllers.count <= 1);
	self.navBar.tabShown = tabShown;
	UIViewController *viewController = [self.viewControllers lastObject]
										?: self.visibleViewController
										?: nil;
	NSString *title = viewController.title ?: @"";
	NSString *footer = viewController.navigationFooter ?: @"";
	self.navBar.footer = footer;
	[self setTitle:title];
}

- (void)viewWillLayoutSubviews {
	[super viewWillLayoutSubviews];
}

- (void)interactivePopGestureRecognizerAction:(UIGestureRecognizer *)sender {
	self.gestureActive = !(
		sender.state == UIGestureRecognizerStateEnded ||
		sender.state == UIGestureRecognizerStateFailed ||
		sender.state == UIGestureRecognizerStateCancelled
	);
	if (sender.state == UIGestureRecognizerStateBegan) {
		if (self.viewControllers.count <= 1) {
			[sender cancelsTouchesInView];
		}
	}
	else if (sender.state == UIGestureRecognizerStateEnded) {
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			[self setTabState];
		});
	}
	else if (sender.state == UIGestureRecognizerStateChanged && self.viewControllers.count > 1) {
		self.navBar.tabShown = NO;
	}
}

- (void)updateNavActionsWithViewController:(nullable UIViewController *)viewController {
	viewController = viewController
		?: [self.viewControllers lastObject]
		?: self.visibleViewController;
	
	if (!viewController) {
		self.navBar.leftButtons = self.leftButtons;
		self.navBar.rightButtons = self.rightButtons;
		return;
	}
	viewController.navigationItem.hidesBackButton = YES;
	[viewController.navigationController.interactivePopGestureRecognizer addTarget:self action:@selector(interactivePopGestureRecognizerAction:)];
	viewController.navigationController.interactivePopGestureRecognizer.delegate = self;
	self.navBar.titleTapAction = viewController.navigationTitleTapAction;
	self.navBar.footer = viewController.navigationFooter ?: @"";
	if (viewController.leftButtons) {
		self.navBar.leftButtons = viewController.leftButtons;
	}
	else if (self.vc_current == viewController && [self.vc_leftButtons count] > 0) {
		self.navBar.leftButtons = self.vc_leftButtons;
	}
	else {
		self.navBar.leftButtons = self.leftButtons;
	}
	
	if (viewController.rightButtons) {
		self.navBar.rightButtons = viewController.rightButtons;
	}
	else if (self.vc_current == viewController && [self.vc_rightButtons count] > 0) {
		self.navBar.rightButtons = self.vc_rightButtons;
	}
	else {
		self.navBar.rightButtons = self.rightButtons;
	}
	[self setTabState];
}

#pragma mark - Getters & Setters

- (UIColor *)navBarBackground {
	return self.navBar.backgroundColor;
}

- (void)setNavBarBackground:(UIColor *)navBarBackground {
	self.navBar.backgroundColor = navBarBackground;
}

- (void)setTitle:(NSString *)title {
	self.navBar.title = title;
}

- (NSString *)title {
	return self.navBar.title;
}

- (TESegmentedControl *)segmentedControl {
	return self.navBar.segmentedControl;
}

- (void)setLeftButtons:(NSArray<UIButton *> *)leftButtons {
	_leftButtons = leftButtons;
	[self updateNavActionsWithViewController:nil];
}

- (void)setRightButtons:(NSArray<UIButton *> *)rightButtons {
	_rightButtons = rightButtons;
	[self updateNavActionsWithViewController:nil];
}

- (void)setVc_leftButtons:(NSArray *)vc_leftButtons {
	_vc_leftButtons = vc_leftButtons;
	[self updateNavActionsWithViewController:nil];
}

- (void)setVc_rightButtons:(NSArray *)vc_rightButtons {
	_vc_rightButtons = vc_rightButtons;
	[self updateNavActionsWithViewController:nil];
}

#pragma mark - Delegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated; {
	if (!viewController.navigationController.interactivePopGestureRecognizer.enabled
		|| viewController.navigationController.interactivePopGestureRecognizer.delegate != self
		|| self.gestureActive == NO
		) {
		self.vc_rightButtons = nil;
		self.vc_leftButtons = nil;
		[self updateNavActionsWithViewController:viewController];
	}
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
	self.vc_rightButtons = nil;
	self.vc_leftButtons = nil;
	[self updateNavActionsWithViewController:viewController];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
	if (self.viewControllers.count <= 1) {
		return nil;
	}
	UIViewController *controller = [super popViewControllerAnimated:animated];
	if (controller) {
		[self setTabState];
	}
	return controller;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	return self.viewControllers.count > 1;
}

@end
