//
//  UIButton+TESegmentedNavigationController.m
//  TESegmentedNavigationController
//
//  Created by Timothy Ellis on 6/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "UIButton+TESegmentedNavigationController.h"

@implementation UIButton (TESegmentedNavigationController)

+ (instancetype)buttonWithTitle:(NSString *)title image:(UIImage *)image target:(id)target action:(SEL)action {
	UIButton *button = [self buttonWithType:UIButtonTypeSystem];
	button.translatesAutoresizingMaskIntoConstraints = NO;
	if (title) {
		title = (image) ? [NSString stringWithFormat:@" %@", title] : title;
		[button setTitle:title forState:UIControlStateNormal];
	}
	[button setImage:image forState:UIControlStateNormal];
	
	button.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
	if (action) {
		[button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	}
	[button sizeToFit];
	return button;
}

+ (instancetype)buttonWithTitle:(NSString *)title target:(id)target action:(SEL)action {
	return [self buttonWithTitle:title image:nil target:target action:action];
}

+ (instancetype)buttonWithImage:(UIImage *)image target:(id)target action:(SEL)action {
	return [self buttonWithTitle:nil image:image target:target action:action];
}

- (void)setHeight:(CGFloat)height {
	CGRect frame = self.frame;
	frame.size.height = height;
	[self setFrame:frame];
}

@end
