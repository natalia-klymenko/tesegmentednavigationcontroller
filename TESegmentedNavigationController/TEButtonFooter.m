//
//  TEButtonFooter.m
//  ButtonFooter
//
//  Created by Timothy Ellis on 11/07/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "TEButtonFooter.h"
#import "NSAttributedString+TESegmentedNavigationController.h"

@interface TEButtonFooter ()
@property (nonatomic, nonnull) NSLayoutConstraint *footerConstraint;
@property (nonatomic, nonnull) UILabel *footerLabel;
@property (nonatomic, nonnull) UIColor *textColor;
@property (nonatomic) BOOL updatesPending;
@end

@implementation TEButtonFooter

@synthesize contentHorizontalAlignment = _contentHorizontalAlignment;

+ (UIFont *)titleFontWithSmallFont:(BOOL)smallFont {
	if (smallFont) {
		return [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
	}
	return [UIFont systemFontOfSize:17.0f weight:UIFontWeightSemibold];
}

- (NSDictionary *)defaultFooterAttributes {
	return @{
			 NSFontAttributeName: [UIFont systemFontOfSize:9.0f],
			 NSForegroundColorAttributeName: [UIColor grayColor],
			 };
}

- (void)setTitle:(NSString *)title {
	_title = title;
	[self shouldUpdateFields];
}

- (void)setFooter:(NSString *)footer {
	_footer = footer;
	[self shouldUpdateFields];
}

- (void)setTitleImage:(UIImage *)titleImage {
	_titleImage = titleImage;
	[self shouldUpdateFields];
}

- (void)setEnabled:(BOOL)enabled {
	self.textColor = enabled ? [self tintColor] : [UIColor blackColor];
	[super setEnabled:enabled];
	[self shouldUpdateFields];
}

- (void)setUpdatesEnabled:(BOOL)updatesEnabled {
	_updatesEnabled = updatesEnabled;
	[self shouldUpdateFields];
}

- (void)shouldUpdateFields {
	if (self.updatesEnabled) {
		[self updateFields];
	}
}

- (void)setContentHorizontalAlignment:(UIControlContentHorizontalAlignment)contentHorizontalAlignment {
	_contentHorizontalAlignment = contentHorizontalAlignment;
	[self updateFields];
}

- (void)updateFields {
	NSMutableDictionary *attributes = [NSMutableDictionary new];
	if (self.enabled) {
		attributes[NSForegroundColorAttributeName] = [self tintColor];
	}
	BOOL footerExists = (self.footer && [self.footer length] > 0);
	attributes[NSFontAttributeName] = [[self class] titleFontWithSmallFont:footerExists];
	if (footerExists) {
		self.contentEdgeInsets = UIEdgeInsetsMake(-self.footerLabel.frame.size.height - 4.0, 0.0, 0.0, 0.0);
		NSMutableDictionary *footerAttributes = [[self defaultFooterAttributes] mutableCopy];
		[footerAttributes addEntriesFromDictionary:self.footerAttributes ?: @{}];
		self.footerLabel.attributedText = [[NSAttributedString alloc] initWithString:self.footer
																		  attributes:footerAttributes
										   ];
	}
	else {
		self.contentEdgeInsets = UIEdgeInsetsZero;
		self.footerLabel.text = @"";
	}

	NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self.title ?: @""
																				   attributes:attributes
											 ];
	self.imageEdgeInsets = UIEdgeInsetsMake(0, // Top
											(self.titleImage == nil) ? 0.0 : -6.0, // Left
											0.0, // Bottom
											0.0 // Right
											);
	[self setAttributedTitle:attrString forState:UIControlStateNormal];
	[self setImage:self.titleImage forState:UIControlStateNormal];
	[self setNeedsLayout];
}

- (NSArray *)horizontalConstraintsForViews:(NSArray <UIView *> *)views {
	NSMutableArray *constraints = [NSMutableArray new];
	for (UIView *view in views) {
		[constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[view]-|"
																				 options:0
																				 metrics:nil
																				   views:NSDictionaryOfVariableBindings(view)
										  ]
		 ];
	}
	return constraints;
}

- (void)commonInit {
	// To move the image to the right side we flip the entire contents
	self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
	
	// Then we have to flip each item individually again to make them back to normal
	self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
	self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
	
	self.footerLabel = [UILabel new];
	self.footerLabel.textAlignment = NSTextAlignmentCenter;
	self.footerLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:self.footerLabel];
	
	for (UIView *view in self.subviews) {
		view.transform = CGAffineTransformMakeScale(-1.0, 1.0);
	}
	
	[NSLayoutConstraint activateConstraints: [self horizontalConstraintsForViews:@[self.footerLabel]]];
	self.footerConstraint = [NSLayoutConstraint constraintWithItem:self.footerLabel
														 attribute:NSLayoutAttributeBottom
														 relatedBy:NSLayoutRelationEqual
															toItem:self
														 attribute:NSLayoutAttributeBottom
														multiplier:1.0
														  constant:0.0
							 ];
	self.footerConstraint.active = YES;
	
	self.updatesEnabled = YES;
	self.updatesPending = NO;
	self.textColor = self.enabled ? [self tintColor] : [UIColor blackColor];
}

- (instancetype)init {
	if (self = [super init]) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	if (self = [super initWithCoder:aDecoder]) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
		[self commonInit];
	}
	return self;
}

+ (instancetype)buttonWithFooter:(NSString *)footer {
	TEButtonFooter *button = [TEButtonFooter buttonWithType:UIButtonTypeCustom];
	return button;
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType {
	TEButtonFooter *button = [super buttonWithType:buttonType];
	return button;
}

- (void)prepareForInterfaceBuilder {
	[super prepareForInterfaceBuilder];
	[self commonInit];
}

@end
